function string1(str) {
  if (str == undefined) {
    return 0;
  }
  if (str.length == 0) {
    return 0;
  }
  let newStr = "";
  for (let i = 0; i < str.length; i++) {
    if (str[i] == "$") {
      continue;
    }
    if (str[i] == ",") {
      continue;
    }
    newStr += str[i];
  }

  if (isNaN(newStr)) {
    return 0;
  } else {
    return parseFloat(newStr);
  }
}
module.exports = string1;
