function string5(str) {
  let answer = "";
  if (str.length == 0) {
    return 0;
  }
  if (str == undefined) {
    return 0;
  }
  for (let index = 0; index < str.length; index++) {
    answer += str[index] + " ";
  }
  return answer.trim(); // remove unusual space;
}

module.exports = string5;
