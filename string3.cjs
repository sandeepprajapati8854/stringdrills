function string3(str) {
  if (str == undefined) {
    return 0;
  }
  if (str.length == 0) {
    return 0;
  }
  let monthName = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  let splitDate = str.split("/");
  let monthNameInDates = splitDate[1];
  return monthName[monthNameInDates - 1];
}
module.exports = string3;
