function string2(str) {
  if (str == undefined) {
    return [];
  }
  let arrIPAddress = str.split(".");
  if (arrIPAddress.length !== 4) {
    return [];
  }
  let ipV4 = [];
  for (let i = 0; i < arrIPAddress.length; i++) {
    ipV4.push(Number(arrIPAddress[i]));
  }
  //   console.log(ipV4);
  for (let num of ipV4) {
    if (isNaN(num) || Number(num) < 0 || Number(num) > 255) return [];
  }
  return ipV4;
}

module.exports = string2;
